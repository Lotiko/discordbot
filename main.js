const { Client, RichEmbed } = require('discord.js');
const fetch = require('node-fetch');
const client = new Client();
require('dotenv').config()

/////////////functions and constants
let counterQuestions = 0;
let currentReponse = "";
const commandRegex = [
    /\!bienvenue/gmi,
    /balek/gmi,
    /\!warning/gmi,
    /fear/gmi,
    /\!quiz/gmi,
    /\!help/gmi,
    /\!joke/,
];
const fearGifs = [
    "https://tenor.com/view/spongebob-scared-gif-4637762 ",
    "https://tenor.com/view/spongebob-squarepants-spongebob-nail-bite-nervous-afraid-gif-3572009 "
];
const balekGifs = [
    "https://tenor.com/view/racoon-wiggle-nuts-balls-gif-8085185 ",
    "https://tenor.com/view/balkany-patrick-balkany-balek-dance-danse-gif-17580608 ",
    "https://tenor.com/view/deez-nuts-balls-scrotum-gif-14600346 "
];
const helpMessage = "Listes des commandes:\n" +
    "!help  => ce message\n     Cette commande si utilisée avec une autre ne renverrat que ce message.\n     Les autres sont cumulative\n" +
    "!bienvenue => Le bot Répond 'Salut les fous!'\n" +
    "balek => renvoie un gif aléatoire compris dans un array balekGifs\n" +
    "fear => renvoie un gif aléatoire compris dans un array fearGifs\n" +
    "!quiz => Un quiz de trois questions\n" +
    "!joke => Bientot Une blague aléatoire de blagues API  https://www.blagues-api.fr/ \n" +
    "!vdm => j'y réfléchit\n\n" +
    "Une envie une suggestions viens en parler sur test-bot\n";

function randomRender(arrayOfGifs) {
    let index = Math.floor(Math.random() * Math.floor(arrayOfGifs.length));
    console.log(index);
    return arrayOfGifs[index];
}
async function mixAnswers(arrayBadAnswers, goodAnswer) {
    let index = Math.floor(Math.random() * Math.floor(arrayBadAnswers.length));
    let i = 0;
    let ret = [];
    while (i < index) {
        ret.push(arrayBadAnswers[i]);
        i++;
    }
    ret.push(goodAnswer);
    console.log(ret);
    if (index !== arrayBadAnswers.length) {
        while (i < arrayBadAnswers.length) {
            ret.push(arrayBadAnswers[i])
            i++;
        }
    }
    return ret;

}
function renderError(message, error) {
    message.channel.send(`Arf Y'a un probléme!`);
    console.log(error)
}
async function renderAnswer(message, answer) {
    setTimeout(() => { message.channel.send(`La réponse est : \n ${answer}`); }, 17000);
}
async function questionProcess(message, questions) {
    const question = questions[counterQuestions];
    currentReponse = question.correct_answer;
    console.log(question);
    console.log(currentReponse);
    if (currentReponse === "True" || currentReponse === "False") {
        message.channel.send(`\nCatégorie: ${question.category}\nQuestions: ${question.question}\nPossible réponses: vrai ou faux\n`);
    } else {
        try {
            let reponses = await mixAnswers(question.incorrect_answers, currentReponse);
            message.channel.send(`\nCatégorie: ${question.category}\nQuestions: ${question.question}\nPossible réponses:\n${reponses.join("\n")}`);
        } catch (error) {
            renderError(message, error);
        }
    }
    renderAnswer(message, currentReponse);
    counterQuestions++;
}
function isCommand(str) {
    let ret = [];
    let i = 0;
    while (i < commandRegex.length) {
        let strMatch = str.match(commandRegex[i]);
        if (strMatch !== null) {
            ret.push(strMatch);
        }
        i++;
    }
    return ret;
}
async function quiz(message) { 
    counterQuestions = 0;
    try {
        const data = await fetch("https://opentdb.com/api.php?amount=3")
            .then(response => { return response.json(); });
        const questions = data.results;
        await questionProcess(message, questions);
        let interval = setInterval( async function() {
            if (counterQuestions === questions.length - 1 && typeof interval !== "undefined") {
                clearInterval(interval);
            }
            try {
                await questionProcess(message, questions);
            } catch (error) {
                console.log(error);
            }          
        }
           , 20000);
    } catch (error) {
        console.log(error);
    }
}
//////////////////////////////////////
//process

client.on("ready", () => {
    client.user.setUsername("plop")
        .then(console.log("C'est tipar!"))
        .catch(console.error())
})
client.on("message", async (message) => {
    if (!message.author.bot) {
        let currentCommands = isCommand(message.content);
        let messRet = [];
        let uniqueCommand = false;
        for (const command of currentCommands) {
            switch (command[0]) {
                case "!help":
                    messRet = [helpMessage];
                    uniqueCommand = true;
                    break;

                case "!bienvenue":
                    messRet.push("Salut les fous!\n");
                    break;

                case "balek":
                    messRet.push(randomRender(balekGifs));
                    break;

                case "fear":
                    messRet.push(randomRender(fearGifs));
                    break;

                case "!quiz":
                    try {
                        await quiz(message);
                    } catch (error) {
                        renderError(message, error);
                    }
                    
                    break;

                default:
                    break;
            }
            if (uniqueCommand) { break; }
        }
        message.channel.send(messRet.join('\n'));
    }
});


client.login(process.env.TOKEN_BOT)
